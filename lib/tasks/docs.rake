namespace :docs do
  desc "Generating docs for project"
  task generate: :environment do
    # system 'echo "Routes for TwittJoin:\n`bundle exec rake routes`\n" > doc/ROUTES'
    system "bundle exec rdoc ./app/controllers/ doc/ROUTES --visibility=public -o public/docs"
  end

  desc "Cleaning docs for project"
  task clean: :environment do
    system "rm -r public/docs"
  end

end
