Rails.application.config.middleware.use OmniAuth::Builder do
  provider :twitter, Rails.application.secrets.twitter_key, Rails.application.secrets.twitter_secret, strategy_class: OmniAuth::Strategies::Twitter
end