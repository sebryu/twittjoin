# Główny kontroler aplikacji, z którego dziedziczą kolejne kontrolery
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :twitter_client
  
  # :nodoc:
  rescue_from CanCan::AccessDenied do |exception|  # :nodoc:
    redirect_to root_url, :alert => exception.message
  end
  
  # Metoda tworząca z danych zapisanych w sesji obiekt klienta Twitterowego
  def twitter_client
    @twitter_client ||= Twitter::REST::Client.new(session["twitter_client"])
  end
end
