# Kontroler początkowy, do akcji nie związanych z żadnym modelem
class HomeController < ApplicationController
  
  # GET /
  #
  # Wyświetla stronę główną na której można się zalogować lub przejść do listy wydarzeń
  def index
  end
end
