module Users  # :nodoc:
  # Kontroler odpowiada za callbacki omniauthowe, dziedziczy z Devisowego OmniauthCallbacksController'a
  class OmniauthCallbacksController < Devise::OmniauthCallbacksController

    # Metoda do logowania się za pomocą twittera przez omniauth
    def twitter
      # You need to implement the method below in your model (e.g. app/models/user.rb)
      @user = User.from_omniauth(request.env["omniauth.auth"].except!("extra"))

      session["twitter_client"] = Twitter::REST::Client.new do |config|
        config.consumer_key = Rails.application.secrets.twitter_key
        config.consumer_secret = Rails.application.secrets.twitter_secret
        config.access_token = request.env["omniauth.auth"].credentials.token
        config.access_token_secret = request.env["omniauth.auth"].credentials.secret
      end
      if @user.persisted?
        sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
        set_flash_message(:notice, :success, :kind => "Twitter") if is_navigational_format?
      else
        session["devise.twitter_data"] = request.env["omniauth.auth"]
        redirect_to new_user_registration_url
      end
    end
  end
end