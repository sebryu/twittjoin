# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime
#  updated_at             :datetime
#  provider               :string
#  uid                    :string
#  nick                   :string
#  avatar_url             :string
#  role                   :string
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :rememberable, 
        :trackable, :validatable, :omniauthable, omniauth_providers: [:twitter]

  has_many :events
  ROLES = %w(creator commenter)
  validates :role, inclusion: { in: ROLES }

  ROLES.each do |some_role|
    define_method "#{some_role}?" do
      self.role == some_role
    end
  end

  def self.email_for_twitter(auth)
    auth.info.nickname + "@twitter.com"
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email || self.email_for_twitter(auth)
      user.password = Devise.friendly_token[0,20]
      user.nick = auth.info.nickname #or name   # assuming the user model has a name
      user.avatar_url = auth.info.image
      user.role = ROLES.second
    end
  end
end
