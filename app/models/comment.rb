# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  event_id   :integer
#  body       :string
#  tweet_url  :string
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Comment < ActiveRecord::Base
  belongs_to :event
  belongs_to :user
  alias_attribute :text, :body

  def hashtag
    event.hashtag
  end

  def tweet_message
    "##{self.hashtag} #{self.body} #{Rails.application.routes.url_helpers.event_comment_url(self.event, self)}"
  end
end
