# == Schema Information
#
# Table name: events
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  city        :string
#  hashtag     :string
#  date        :datetime
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Event < ActiveRecord::Base
  belongs_to :user
  has_many :comments

  def tweet_message
    "Wydarzenie: #{name}, zobacz na: #{Rails.application.routes.url_helpers.event_url(self)}"
  end
  
end
