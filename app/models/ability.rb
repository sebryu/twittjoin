class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end

    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    user ||= User.new
    can :read, :all
    return unless user.persisted?
    can :publish, :all
    if user.creator?
        can :manage, :all
        cannot [:edit, :destroy], Event
        can [:edit, :destroy], Event, user: user
        
    elsif user.commenter?
        can :manage, :all
        cannot [:create, :edit, :destroy], Event
    end
  end
end
