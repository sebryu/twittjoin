class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :event, index: true
      t.string :body
      t.string :tweet_url
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :comments, :events
    add_foreign_key :comments, :users
  end
end
